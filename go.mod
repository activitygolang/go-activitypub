module gitlab.com/activitygolang/go-activitypub

go 1.13

require (
	github.com/go-oauth2/oauth2 v3.9.2+incompatible
	github.com/go-oauth2/oauth2/v4 v4.2.0
	github.com/mattn/go-sqlite3 v1.14.6 // indirect
	gitlab.com/activitygolang/go-activitystreams v0.0.0-20210719154700-cea383b246ad
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
)
