package apnet

import (
	"fmt"
	"time"
	"net/http"
	goap "gitlab.com/activitygolang/go-activitystreams"
	goobj "gitlab.com/activitygolang/go-activitystreams/object"
)

var form = `
<form>
Email:<br>
<input type="text" name="email"><br>
Password:<br>
<input type="password" name="password"><br>
Actor name:<br>
<input type="text" name="name"><br>
Actor type:<br>
<input type="radio" name="type" value="Person"> Person<br>
<input type="radio" name="type" value="Group"> Group<br>
<input type="radio" name="type" value="Application"> Application<br>
<input type="radio" name="type" value="Service"> Service<br>
<input type="radio" name="type" value="Organization"> Organization<br>
<input type="submit" value="AddActor">
</form>
`

var header = `
<!DOCTYPE html>
<html>
<body>
`

var footer = `
</body>
</html>
`

// TODO: REMOVE ADDACTOR DUMMY
func (l *Listener) AddActor(resp http.ResponseWriter, req *http.Request) {
	actorName := req.FormValue("name")
	actorType := req.FormValue("type")
	email := req.FormValue("email")
	password := req.FormValue("password")
	// did we get a valid form?
	extraData := ""
	valid := actorName != "" && email != "" && password != "" && goap.IsActor(actorType)
	if valid {
		// create account
		// hash new password
		salt, hashed, err := HashNewPassword(password)
		if err == nil {
			// Create timestamp
			now := time.Now()
			datetime := now.Format(time.RFC3339Nano)
			// assemble account structure
			acct := goap.Account{
				User: email,
				Salt: salt,
				Time: HASH_TIME,
				Memory: HASH_MEMORY,
				KeyLen: HASH_KEYLEN,
				HashPW: hashed,
				ActorID: goobj.LocalID(actorName),
				CreationDate: datetime,
			}
			// new account
			err := l.core.NewAccount(actorName, actorType, acct)
			if err != nil {
				extraData = fmt.Sprintln("Error:", err)
			}
		} else {
			extraData = fmt.Sprintln("Error:", err)
		}
		// add result line to response
		extraData = fmt.Sprintf("Got name: %s, type: %s<br>",
			actorName, actorType)
	}
	responseData := header + extraData + "\n" + form + footer
	// Display form
	fmt.Fprintln(resp, responseData)
}

// Submit form
//  get form data
//   name
//  do core add actor
