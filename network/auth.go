package apnet

import (
	"fmt"
	"errors"
	"net/http"
	"strconv"
	"encoding/json"
	"crypto/subtle"
	"crypto/rand"
	
	"golang.org/x/crypto/argon2"

	//"github.com/go-oauth2/oauth2/v4/errors"
	"github.com/go-oauth2/oauth2/v4/manage"
	"github.com/go-oauth2/oauth2/v4/models"
	"github.com/go-oauth2/oauth2/v4/server"
	"github.com/go-oauth2/oauth2/v4/store"
)

const HASH_TIME = 1
const HASH_MEMORY = 64*1024
const HASH_KEYLEN = 32

var SALT_LEN_FAIL = errors.New("Failed to generate salt of proper length")


// We are keeping the same endpoints as mastodon to ease compatability
// /oauth/token
// /oauth/authorize?

// import the go-oauth2 or whatever server library
// wrap it up in simple functions

type OauthData struct {
	Manager *manage.Manager
	ClientStore *store.ClientStore
	Server *server.Server
	nextClientID int
}

func InitOauth(pwhandler server.PasswordAuthorizationHandler) (o *OauthData) {
	o = new(OauthData)
	// manager
	o.Manager = manage.NewDefaultManager()
	o.Manager.MustTokenStorage(store.NewMemoryTokenStore())
	// client store
	o.ClientStore = store.NewClientStore()
	o.Manager.MapClientStorage(o.ClientStore)
	// server
	o.Server = server.NewDefaultServer(o.Manager)
	o.Server.SetAllowGetAccessRequest(true)
	o.Server.SetClientInfoHandler(server.ClientFormHandler)
	o.Server.SetPasswordAuthorizationHandler(pwhandler)
	return o
}

func (o *OauthData) HandleNewClient(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		return
	}
	// generate a client id
	clientID := strconv.Itoa(o.nextClientID)
	o.nextClientID++
	// generate a client secret
	clientSecret := "TODO"
	// extract domain
	domain := r.URL.Host
	// assemble and store
	o.ClientStore.Set(clientID, &models.Client{
		ID: clientID,
		Secret: clientSecret,
		Domain: domain,
		// no user id
	})
	// return id and secret
	data := map[string]string{
		"client_id": clientID,
		"client_secret": clientSecret,
	}
	blob, err := json.Marshal(data)
	if err != nil {
		// TODO: json error
		fmt.Println("handleNewClient json.Marshal error:", err)
	}
	// header
	w.WriteHeader(200)
	// insert into packet body
	w.Write(blob)
}

func (o *OauthData) HandleAuthorize(w http.ResponseWriter, r *http.Request) {
	fmt.Println("HandleAuthorize")
	err := o.Server.HandleAuthorizeRequest(w, r)
	if err != nil {
		fmt.Println("HandleAuthorize error:", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
}

func (o *OauthData) HandleToken(w http.ResponseWriter, r *http.Request) {
	err := o.Server.HandleTokenRequest(w, r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
}

func HashNewPassword(pw string) (salt [16]byte, hashed []byte, err error) {
	// generate salt
	n, err := rand.Reader.Read(salt[:])
	if err != nil {
		return [16]byte{}, nil, err
	}
	if n != len(salt) {
		return [16]byte{}, nil, SALT_LEN_FAIL
	}
	// hash
	hashed = argon2.IDKey([]byte(pw), salt[:], 1, 64*1024, 1, 32)
	// return salt and hashed
	return salt, hashed, nil
}

func CheckPasswordHash(checkee string, salt [16]byte, hashed []byte, time, memory, keyLen uint32) (valid bool) {
	// hash salt and checkee
	checkeeHash := argon2.IDKey([]byte(checkee), salt[:], time, memory, 1,
		keyLen)
	// constant compare checkee-hash and hash
	if subtle.ConstantTimeCompare(checkeeHash, hashed) == 1 {
		return true
	} else {
		return false
	}
}
