package apnet

import (
	"fmt"
	"net/http"
	"strings"
	goobj "gitlab.com/activitygolang/go-activitystreams/object"
)

func (l *Listener) get_vc_data(userID string) (vc map[string]interface{}, err error) {
	// load account
	acct, err := l.core.LoadAccount(userID)
	if err != nil {
		// failure; bail and error
	}
	// load actor
	sAID := string(acct.ActorID)
	localActor, err := l.core.GetLocal(sAID, "")
	fmt.Println(localActor) // COMPILER SHUTTER UPPER
	if err != nil {
		// failure; bail and error
	}
	//   but in what form? need
	// load secondaries
	// mung up the dict of doom
	username := sAID[strings.LastIndex(sAID, "/")+1:]
	vcdata := map[string]interface{}{
		"id": nil,
		"username": username,
		"acct": username,
		"display_name": nil,
		"locked": nil,
		"bot": nil,
		"created_at": acct.CreationDate,
		"note": nil,
		"url": nil,
		"avatar": nil,
		"avatar_static": nil,
		"header": nil,
		"header_static": nil,
		"followers_count": nil,
		"following_count": nil,
		"statuses_count": nil,
		"last_status_at": nil,
		"source": map[string]interface{}{
		},
		"emojis": map[string]interface{}{
		},
		"fields": map[string]interface{}{
		},
	}
	return vcdata, nil
}

func (l *Listener) verify_credentials(resp http.ResponseWriter, req *http.Request) {
	fmt.Println("verify_credentials")
	// will get a user token
	// feed token into oauth
	tokenInfo, err := l.oauth.Server.ValidationBearerToken(req)
	if err != nil {
		// failure; bail and error
	}
	// get user id
	userID := tokenInfo.GetUserID()
	vc, err := l.get_vc_data(userID) // TODO: ERR
	if err != nil {
		fmt.Println("get_vc_data error:", err)
	}
	data, err := goobj.EncodeJSON(vc) // TODO: ERR
	if err != nil {
		fmt.Println("EncodeJSON error:", err)
	}
	fmt.Println(string(data))
	// return
	resp.WriteHeader(200)
	resp.Write(data)
}
