package apnet

import (
	"bytes"
	"fmt"
	goap "gitlab.com/activitygolang/go-activitystreams"
	goobj "gitlab.com/activitygolang/go-activitystreams/object"
	"io/ioutil"
	"net/http"
	"strings"
	"crypto/tls"
)

type APInterface interface {
	Get(objectID string, authActorID string) (map[string]interface{}, error)
	GetLocal(objectID string, authActorID string) (map[string]interface{}, error)
	PostInbox(targetID string, object map[string]interface{}) error
	PostOutbox(outboxID string, object map[string]interface{}) error
	NewAccount(actorID string, actorType string, acct goap.Account) error
	LoadAccount(user string) (goap.Account, error)
	UpdateAccount(user string, acct goap.Account) error
	DeleteAccount(user string) error
}

// Listener must do the upper level checks and conversions for GoAP
type Listener struct {
	core APInterface
	oauth *OauthData
}

func NewListener(inter APInterface) (l *Listener) {
	l = &Listener{}
	l.core = inter
	l.oauth = InitOauth(l.handlePassword)
	return l
}

func (l *Listener) ServeHTTP(resp http.ResponseWriter, req *http.Request) {
	fmt.Println("REQUEST:", req.Method, req.URL.Path)
	// First check for an AddActor command
	if strings.ToLower(req.URL.Path) == "/addactor" {
		l.AddActor(resp, req)
		return
	}
	// ============ NEW FILTER
	// check paths to split into
	compPath := strings.ToLower(req.URL.Path)
	if strings.HasPrefix(compPath, "/api/v1/apps") {
		// create app
		l.oauth.HandleNewClient(resp, req)
		return
	} else if strings.HasPrefix(compPath, "/api/v1/instance") {
		l.instanceInfo(resp, req)
		return
	} else if strings.HasPrefix(compPath, "/oauth/token") {
		l.oauth.HandleToken(resp, req)
		return
	} else if strings.HasPrefix(compPath, "/oauth/authorize") {
		l.oauth.HandleAuthorize(resp, req)
		return
	} else if strings.HasPrefix(compPath, "/api/v1/accounts/verify_credentials") {
		// what data do we need to feed in?
		// will be a user token
		l.verify_credentials(resp, req)
		return
	} else if strings.HasPrefix(compPath, "/ap/") {
		// ActivityPub direct
		// NOT YET AUTHENTICATED
		switch req.Method {
		case "GET":
			// if a get, send to coreinterface.Get
			l.handleGet(resp, req)
		case "POST":
			// if a post, send to coreinterface.PostInbox
			l.handlePost(resp, req)
		default:
			fmt.Println("unknown method", req.Method)
		}
	}
}

func (l *Listener) handleGet(resp http.ResponseWriter, req *http.Request) {
	fmt.Println("Get")
	path := req.URL.Path
	// do top level goap sanity checks
	// get object
	obj, err := l.core.Get(path, "testAuthID")
	if err != nil {
		// TODO: error
		fmt.Println("Get error:", err)
		if err == goobj.NoObject { // return 404
			resp.WriteHeader(404)
			resp.Write([]byte("No object found"))
		}
		return
	}
	// serialize object into body and return
	data, err := goobj.EncodeJSON(obj)
	if err != nil {
		// TODO: error
		fmt.Println("Encode error:", err)
		return
	}
	resp.Header().Add("Content-Type", "application/activity+json")
	resp.WriteHeader(200)
	resp.Write(data)
}

func (l *Listener) handlePost(resp http.ResponseWriter, req *http.Request) {
	fmt.Println("Post")
	// parse into map[string]interface
	path := req.URL.Path
	// get body
	if req.Body == nil {
		// TODO: error
		fmt.Println("No body")
		return
	}
	data, err := ioutil.ReadAll(req.Body)
	if err != nil {
		// TODO: error
		fmt.Println("Body read error:", err)
		return
	}
	obj, err := goobj.DecodeJSON(data)
	if err != nil {
		// TODO: error
		fmt.Println("Decode error:", err)
		return
	}
	// do top level goap sanity checks
	// determine if it is targeting an outbox
	if strings.HasSuffix(path, "outbox") || strings.HasSuffix(path, "outbox/") {
		err = l.core.PostOutbox(path, obj)
	} else {
		err = l.core.PostInbox(path, obj)
	}
	fmt.Println("post error:", err)
	// result, error checks
}

func (l *Listener) instanceInfo(w http.ResponseWriter, r *http.Request) {
	data := `{"title":"GoAP", "version":"3.0.1"}`
	w.WriteHeader(200)
	w.Write([]byte(data))
	return
}

func (l *Listener)handlePassword(username, password string) (userID string, err error) {
	fmt.Println("handlePassword:", username, password)
	// reach into core, load password
	acct, err := l.core.LoadAccount(username)
	if err != nil {
		return "", err
	}
	// get hash of provided password
	valid := CheckPasswordHash(password, acct.Salt, acct.HashPW, acct.Time, acct.Memory, acct.KeyLen)
	if valid {
		fmt.Println("  valid:", acct.ActorURL)
		return acct.ActorURL, nil
	} else {
		// no userID means invalid login
		fmt.Println("  invalid")
		return "", nil
	}
}

// ---- HTTPSClient ---- //

type HTTPSClient struct {
	client *http.Client
}

func NewHTTPSClient(insecure bool) (h *HTTPSClient) {
	h = &HTTPSClient{}
	config := &tls.Config{
		InsecureSkipVerify: insecure,
	}
	tr := &http.Transport{TLSClientConfig: config}
	h.client = &http.Client{Transport: tr}
	return h
}

func (h *HTTPSClient) SendToServer(url string, object map[string]interface{}) (err error) {
	// url will point to the actor
	actor, err := h.getServerObject(url)
	if err != nil {
		return err
	}
	// get the actor, extract inbox
	inbox, err := goobj.GetStr(actor, "inbox")
	if err != nil {
		return err
	}
	// post to inbox
	err = h.postServerObject(inbox, object)
	if err != nil {
		return err
	}
	return nil
}

func (h *HTTPSClient) getServerObject(url string) (object map[string]interface{}, err error) {
	resp, err := h.client.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	obj, err := goobj.DecodeJSON(data)
	if err != nil {
		return nil, err
	}
	return obj, nil
}

func (h *HTTPSClient) postServerObject(url string, object map[string]interface{}) (err error) {
	data, err := goobj.EncodeJSON(object)
	if err != nil {
		return err
	}
	// turn data into reader
	reader := bytes.NewReader(data)
	resp, err := h.client.Post(url, "application/json", reader)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	return nil
}
