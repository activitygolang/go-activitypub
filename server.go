package main

import (
	"os"
	"fmt"
	"time"
	"strings"
	"strconv"
	"net/http"
	goap "gitlab.com/activitygolang/go-activitystreams"
	goobj "gitlab.com/activitygolang/go-activitystreams/object"
	godb "gitlab.com/activitygolang/go-activitystreams/database"
	"gitlab.com/activitygolang/go-activitypub/network"
	"flag"
)

// What is needed here:
//  * Interface with the AP core
//  * Dethreading layer over the interface
//  * Code to connect to other servers
//  * Network listener

// AP core interface requirements:
//  * conform to UpperLevel interface (and have some way of sending that data)

// Dethreading layer requirements:
//  * make sure the thing can't stall out

// Network sender requirements:

// Network listener requirements:
//  * Encapsulate network centric layers (TLS, auth)
//  * Do network level sanity checks on incoming objects


func main() {
	var port int
	var host string
	var dbpath string
	var certPath, keyPath string
	var insecure bool
	flag.IntVar(&port, "p", 0, "Port to listen on.")
	flag.StringVar(&host, "h", "", "Hostname to use.")
	flag.StringVar(&dbpath, "d", "", "Path of database file.")
	flag.StringVar(&certPath, "c", "", "Path of cert file.")
	flag.StringVar(&keyPath, "k", "", "Path of key file.")
	flag.BoolVar(&insecure, "insecure-ssl", false,
		"Accept/Ignore all server SSL certificates")
	flag.Parse() 
	isNew := false
	// check dbpath exists
	if _, err := os.Stat(dbpath); os.IsNotExist(err) {
		isNew = true
	}
	// set up core
	core := goap.NewCore() // apcore
	core.Prefix = "ap"
	if host == "" {
		core.Hostname = "127.0.0.1"
	} else {
		core.Hostname = host
	}
	if port == 0 {
		core.Port = 8080 // TODO: Default port?
	} else {
		core.Port = uint16(port)
	}
	// database
	dbase, err := godb.OpenDatabase(dbpath)
	if err != nil {
		fmt.Println("OpenDatabase error:", err)
		return
	}
	if isNew {
		err = dbase.Initialize()
		if err != nil {
			fmt.Println("Initialize new database error:", err)
		}
	}
	dbase.DebugLevel = godb.DEEPDEBUG
	core.SetDatabase(dbase)
	// check for ipv6 style ip
	if strings.Contains(core.Hostname, ":") {
		if !strings.HasPrefix(core.Hostname, "[") {
			core.Hostname = "[" + core.Hostname
		}
		if !strings.HasSuffix(core.Hostname, "]") {
			core.Hostname += "]"
		}
	}
	// set up network listener
	inter := NewCoreInterface(core)
	inter.Start(insecure)
	listener := apnet.NewListener(inter)
	fullAddr := core.Hostname + ":" + strconv.Itoa(int(core.Port))
	err = http.ListenAndServeTLS(fullAddr, certPath, keyPath, listener)
	if err != nil {
		fmt.Println("Listening error:", err)
	}
}

type CallReturn struct {
	Error  error
	ObjectID string
	Object map[string]interface{} // Returned by Get
	Account goap.Account // Returned by LoadAccount
}

type CoreCall struct {
	Type       int // What call is being made?
	User       string
	ObjectID   string
	ActorID    string
	ActorType  string
	Object     map[string]interface{}
	ReturnChan chan CallReturn
	Account    goap.Account
}

type CoreInterface struct {
	apcore  *goap.Core
	calls   chan CoreCall // internal dethreader channel
	Running bool
}

const (
	GET = iota
	GETLOCAL
	POSTOUT
	POSTIN
	NEW_ACCT
	LOAD_ACCT
	UPDATE_ACCT
	DELETE_ACCT
)

func NewCoreInterface(core *goap.Core) (c *CoreInterface) {
	c = &CoreInterface{}
	c.apcore = core
	c.calls = make(chan CoreCall, 128) // hat generated number
	return c
}

func (c *CoreInterface) Start(allowInsecure bool) {
	go c.DethreaderLoop()
	go c.PropagationMonitor(allowInsecure)
}

func (c *CoreInterface) Stop() {
	c.Running = false
}

func (c *CoreInterface) DethreaderLoop() {
	c.Running = true
	for c.Running {
		ret := CallReturn{}
		select {
		case call := <-c.calls:
			fmt.Println("Call", call)
			if c.apcore == nil {
				fmt.Println("Dethreader fail: no apcore")
			}
			switch call.Type {
			case GET:
				// the ObjectID is already a local
				localID := goobj.LocalID(call.ObjectID)
				authID := goobj.LocalID(call.ActorID)
				ret.Object, ret.Error = c.apcore.Get(localID, authID)
			case GETLOCAL:
				// used by info-generators
				localID := goobj.LocalID(call.ObjectID)
				authID := goobj.LocalID(call.ActorID)
				ret.Object, ret.Error = c.apcore.GetLocal(localID, authID)
			case POSTOUT:
				localID := goobj.LocalID(call.ActorID)
				ret.ObjectID, ret.Error = c.apcore.PostOutbox(localID,
					call.Object)
			case POSTIN:
				localID := goobj.LocalID(call.ActorID)
				ret.Error = c.apcore.PostInboxRemote(localID, call.Object)
			case NEW_ACCT:
				// make actor
				localID := goobj.LocalID(call.ActorID)
				ret.Error = c.apcore.AddActor(localID, call.ActorType)
				if ret.Error != nil {
					break
				}
				// make account
				acct := call.Account
				ret.Error = c.apcore.NewAccount(acct)
			case LOAD_ACCT:
				user := call.User
				ret.Account, ret.Error = c.apcore.LoadAccount(user)
			case UPDATE_ACCT:
				user := call.User
				acct := call.Account
				ret.Error = c.apcore.UpdateAccount(user, acct)
			case DELETE_ACCT:
				user := call.User
				ret.Error = c.apcore.DeleteAccount(user)
			}
			call.ReturnChan <- ret
		}
	}
}

func (c *CoreInterface) Get(objectID string, authActorID string) (object map[string]interface{}, err error) {
	retChan := make(chan CallReturn, 1)
	call := CoreCall{
		Type: GET,
		ObjectID: objectID,
		ActorID: authActorID,
		ReturnChan: retChan,
	}
	c.calls <- call
	result := <-retChan
	return result.Object, result.Error
}

func (c *CoreInterface) GetLocal(objectID string, authActorID string) (object map[string]interface{}, err error) {
	retChan := make(chan CallReturn, 1)
	call := CoreCall{
		Type: GETLOCAL,
		ObjectID: objectID,
		ActorID: authActorID,
		ReturnChan: retChan,
	}
	c.calls <- call
	result := <-retChan
	return result.Object, result.Error
}

func (c *CoreInterface) PostOutbox(outboxID string, object map[string]interface{}) (err error) {
	retChan := make(chan CallReturn, 1)
	call := CoreCall{
		Type: POSTOUT,
		ActorID: outboxID,
		Object: object,
		ReturnChan: retChan,
	}
	c.calls <- call
	result := <-retChan
	return result.Error
}

func (c *CoreInterface) PostInbox(targetID string, object map[string]interface{}) (err error) {
	retChan := make(chan CallReturn, 1)
	call := CoreCall{
		Type: POSTIN,
		ActorID: targetID,
		Object: object,
		ReturnChan: retChan,
	}
	c.calls <- call
	result := <-retChan
	return result.Error
}

func (c *CoreInterface) NewAccount(actorID string, actorType string, acct goap.Account) (err error) {
	retChan := make(chan CallReturn, 1)
	call := CoreCall{
		Type: NEW_ACCT,
		ActorID: actorID,
		ActorType: actorType,
		Account: acct,
		ReturnChan: retChan,
	}
	c.calls <- call
	result := <-retChan
	return result.Error
}

func (c *CoreInterface) LoadAccount(user string) (acct goap.Account, err error) {
	retChan := make(chan CallReturn, 1)
	call := CoreCall{
		Type: LOAD_ACCT,
		User: user,
		ReturnChan: retChan,
	}
	c.calls <- call
	result := <-retChan
	return result.Account, result.Error
}

func (c *CoreInterface) UpdateAccount(user string, acct goap.Account) (err error) {
	retChan := make(chan CallReturn, 1)
	call := CoreCall{
		Type: UPDATE_ACCT,
		User: user,
		Account: acct,
		ReturnChan: retChan,
	}
	c.calls <- call
	result := <-retChan
	return result.Error
}

func (c *CoreInterface) DeleteAccount(user string) (err error) {
	retChan := make(chan CallReturn, 1)
	call := CoreCall{
		Type: DELETE_ACCT,
		User: user,
		ReturnChan: retChan,
	}
	c.calls <- call
	result := <-retChan
	return result.Error
}

func (c *CoreInterface) PropagationMonitor(allowInsecure bool) {
	client := apnet.NewHTTPSClient(allowInsecure)
	for c.Running {
		time.Sleep(5 * time.Second)
		objects, propagations, err := c.apcore.PendingPropagations()
		if err != nil {
			continue
		}
		// extract due propagations from list
		filtered := getDuePropagations(propagations)
		removals := map[goobj.LocalID][]string{}
		updates := map[goobj.LocalID][]goap.PropRetryTarget{}
		for objID, props := range filtered {
			for _, prop := range props {
				// attempt send
				err := client.SendToServer(prop.Url, objects[objID])
				fmt.Println("SENT:", objID, prop.Url, err)
				if err == nil {
					// add to removal list
					removals[objID] = append(removals[objID], prop.Url)
				} else {
					// update attempt data
					prop.TryCount++
					now := time.Now()
					prop.LastTry = now.Format(time.RFC3339)
					// add to update list
					updates[objID] = append(updates[objID], prop)
				}
			}
		}
		// commit changes
		c.apcore.UpdatePropagation(updates, removals)
	}
}

func getDuePropagations(props map[goobj.LocalID][]goap.PropRetryTarget) (filteredProps map[goobj.LocalID][]goap.PropRetryTarget) {
	filteredProps = make(map[goobj.LocalID][]goap.PropRetryTarget, 0)
	for objID, proplist := range props {
		current := []goap.PropRetryTarget{}
		for _, prop := range proplist {
			// calc time since attempt
			lastTry, err := time.Parse(time.RFC3339, prop.LastTry)
			var due bool
			if err == nil {
				now := time.Now()
				elapsed := now.Sub(lastTry)
				due = elapsed > (5 * time.Minute) // FIXME temp hardwire
			} else { // bad date means immediate retry
				due = true
			}
			// lookup delay for try count
			if due {
				// if due: add to list
				current = append(current, prop)
			}
		}
		if len(current) > 0 {
			filteredProps[objID] = current
		}
	}
	return filteredProps
}
