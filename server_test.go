package main

import (
	"time"
	"testing"
	"reflect"
	goap "gitlab.com/activitygolang/go-activitystreams"
	goobj "gitlab.com/activitygolang/go-activitystreams/object"
)

func Test_getDuePropagations(t *testing.T) {
	// Generate current day timestamps
	temp := time.Now()
	nonexpired := temp.Format(time.RFC3339)
	temp = temp.Add(-1 * 24 * 365 * time.Hour)
	expired := temp.Format(time.RFC3339)
	data := map[goobj.LocalID][]goap.PropRetryTarget{
		"fooid":[]goap.PropRetryTarget{
			goap.PropRetryTarget{
				Url:"goap.elsewhere.test",
				TryCount:0,
				LastTry:nonexpired,
			},
			goap.PropRetryTarget{
				Url:"goap.void.test",
				TryCount:0,
				LastTry:expired,
			},
		},
		"barid":[]goap.PropRetryTarget{
			goap.PropRetryTarget{
				Url:"",
				TryCount:0,
				LastTry:nonexpired,
			},
			goap.PropRetryTarget{
				Url:"",
				TryCount:0,
				LastTry:nonexpired,
			},
		},
	}
	filtered := getDuePropagations(data)
	expected := map[goobj.LocalID][]goap.PropRetryTarget{
		"fooid":[]goap.PropRetryTarget{
			goap.PropRetryTarget{
				Url:"goap.void.test",
				TryCount:0,
				LastTry:expired,
			},
		},
	}
	if !reflect.DeepEqual(filtered, expected) {
		t.Fatal("getDuePropagations failure:", filtered)
	}
}
